﻿// Created by Juan Sebastian Munoz
// naruse@gmail.com
// pencilsquaregames.com

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameGUI : MonoBehaviour {

    public Text score;
    public Text multiplier;

    void Update() {
        score.text = "Score:" + GetComponent<GameManager>().Score;
        multiplier.text = "Multiplier: " + GetComponent<GameManager>().Multiplier + "x";
    }
}
