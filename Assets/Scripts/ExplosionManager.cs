﻿/*
  Small singleton class that loads explosions on a given place.

  // Created by Juan Sebastian Munoz
  // naruse@gmail.com
  // pencilsquaregames.com

  WARN: This consumes a lot of draw calls when an explosion is generated as it creates several cubes "resembling" the bounding box of the object to destroy
 */
using UnityEngine;
using System.Collections;

public class ExplosionManager {

    private static ExplosionManager instance = null;

    private float secondsToDestroy = 0.5f;

    private int objectsSize = 3;
    private float objScale = 0.05f;

    private Material explosionMaterial;

    private ExplosionManager() {
        explosionMaterial = new Material(Shader.Find("Diffuse"));
    }

    public static ExplosionManager Instance {
        get {
            if (instance == null) {
                instance = new ExplosionManager();
            }
            return instance;
        }
    }

    public void CreateExplosion(Vector3 pos, Color explosionColor) {
        GameObject pivot = new GameObject();
        pivot.transform.position = pos;
        pivot.name = "Explosion";
        float offset =  objScale/2 - ((float)objectsSize*objScale)/2;
        for(int i = 0; i < objectsSize; i++) {
            for(int j = 0; j < objectsSize; j++) {
                for(int k = 0; k < objectsSize; k++) {
                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    obj.transform.position = pos + new Vector3(i*objScale + offset,
                                                               j*objScale + offset,
                                                               k*objScale + offset);
                    obj.transform.localScale = new Vector3(objScale, objScale, objScale);
                    obj.renderer.material = explosionMaterial;
                    obj.renderer.material.color = explosionColor;
                    obj.transform.parent = pivot.transform;
                    obj.AddComponent<Rigidbody>();
                    obj.rigidbody.AddExplosionForce(50, pos, objScale*objectsSize, 0);
                }
            }
        }
        GameObject.Destroy(pivot, secondsToDestroy);
    }
}