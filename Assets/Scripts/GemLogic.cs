﻿// Created by Juan Sebastian Munoz
// naruse@gmail.com
// pencilsquaregames.com

using UnityEngine;
using System.Collections;

public class GemLogic : MonoBehaviour {

    public float rotationSpeed = 80;

    private int gemType = 1;
    public int GemType { get { return gemType; } }

    //position in the board logic matrix
    private int posX = -1;
    private int posZ = -1;

    private bool gemIsSelected = false;
    public bool GemIsSelected  { get { return gemIsSelected; } }

    private Color gemColor;
    public Color GemColor  { get { return gemColor; } }

    private GameManager belongingBoard;

    private Quaternion initialRotation;
    void Start() {
        initialRotation = transform.rotation;
    }

    void OnMouseOver() {
        if(Input.GetMouseButtonDown(0)) {
            belongingBoard.SelectAdjacentGems(posX, posZ, gemType);
            belongingBoard.CheckForResetMultiplier();
        }
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }

    void OnMouseExit() {
        transform.rotation = initialRotation;
    }

    public void SetColor(Color c) {
        renderer.material.color = c;
        gemColor = c;
    }

    public void SelectGem(bool paintGem = true) {
        if(paintGem)
            renderer.material.color = Color.gray;
        gemIsSelected = true;
    }

    public void UnSelectGem() {
        renderer.material.color = gemColor;
        gemIsSelected = false;
    }

    public void SetTypeAndReference(int type, GameManager gm) {
        belongingBoard = gm;
        gemType = type;
    }

    public void SetGemPos(int i, int j) {
        posX = i;
        posZ = j;
    }
}
